var express = require("express");
//var http = require("http");

var app = express();

app.set("views", __dirname + "/views");
app.set("view engine", "jade");
app.use(express.static(__dirname + "/public"));

app.get("/", function(req, res){
	res.render("index", { title: "Home" });
});

app.use(function(req, res, next){
	res.status(404).render("error");
});
//var server = http.createServer(app);
app.listen(8080);
console.log("Server running at 127.0.0.1:8080");